<?php
namespace Bleez\Pagseguro\Plugin\Checkout\Block\Checkout;

use Magento\Framework\Indexer\CacheContext;
use Magento\Framework\Event\ManagerInterface as EventManager;


class AttributeMerger
{

    protected $cacheContext;

    protected $eventManager;

    public function __construct(
        CacheContext $cacheContext,
        EventManager $eventManager
    )
    {
        $this->cacheContext = $cacheContext;
        $this->eventManager = $eventManager;

    }

    protected $fieldNames = array('', 'Número', 'Complemento', 'Bairro');

    public function afterMerge(\Magento\Checkout\Block\Checkout\AttributeMerger $subject, $result)
    {
        if(isset($result['street'])){
            $result['street']['config']['template'] = 'Bleez_Pagseguro/group';
            foreach($result['street']['children'] as $k => $child){
                if($k == 0) {
                    $result['street']['children'][$k]['label'] = $result['street']['label']->getText();
                    $result['street']['children'][$k]['additionalClasses'] = 'required';
                }else if($k == 2){
                    $result['street']['children'][$k]['label'] = $this->fieldNames[$k];
                }else{
                    $result['street']['children'][$k]['validation']['required-entry'] = true;
                    $result['street']['children'][$k]['label'] = $this->fieldNames[$k];
                    $result['street']['children'][$k]['additionalClasses'] = 'required';
                }
            }
        }

        if(isset($result['vat_id'])){
            $result['vat_id']['validation']['required-entry'] = true;
            $result['vat_id']['label'] = __('CPF');
        }
        
        if (array_key_exists('street', $result)) {
      		$result['street']['children'][0]['label'] = __('Endereço');
      		
    	}
         if (array_key_exists('region_id', $result)) {
      		$result['region_id']['label'] = __('Estado/Região');
      		
    	}



        return $result;
    }
}
